const http = require('http')

const port = 4000

const server = http.createServer((request, response) => {

	if (request.url == '/profile'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	} else if (request.url == '/courses'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here’s our courses available")
	} else if (request.url == '/addCourse'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Add a course to our resources")
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page is not available')
	}
})
server.listen(port);
console.log(`Server now accessible at localhost: ${"port"}.`)